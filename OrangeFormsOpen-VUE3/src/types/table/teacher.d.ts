export default interface Teacher {
  // 主键Id
  teacherId: undefined;
  // 教师名称
  teacherName: undefined;
  // 教师生日
  birthday: undefined;
  // 教师性别
  gender: undefined;
  // 所教科目
  subjectId: undefined;
  // 教师职级
  level: undefined;
  // 鲜花数量
  flowerCount: undefined;
  // 所属校区
  schoolId: undefined;
  // 绑定用户
  userId: undefined;
  // 入职时间
  registerDate: undefined;
  // 是否在职
  available: undefined;
}
